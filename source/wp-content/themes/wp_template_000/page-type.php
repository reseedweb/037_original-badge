<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="type-top-content">
		<h1 class="sub-top-title"><span class="sub-title-step">Step1</span>タイプを選ぶ</h1>
	</div>
	<div class="sub-top-text">	
		<p>作りたいイメージやデザインからオリジナルピンバッジの製法を選びます。</p>
		<p>これにより仕上がりの質感やデザイン再現度がかなり変わってきます。</p>
		<p>まずはサンプルの写真を参考にお好きなイメージに合うのものをお選びください。製法によって、デザインとの相性がございます。デザイン提出いただいてから、より最適なピンバッジ製法を提案させていただくこともございます。</p>
		<p>デザインがまだできていない場合は、「こんなものを作りたい」というイメージだけでも構いません。どれを選択していいか迷ってしまう方は、スタッフまでお気軽にご相談ください。</p>
	</div>
	<div id="type1" class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">真鍮エッチング</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/type_content1_img1.jpg" alt="type" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/type_content1_img2.jpg" alt="type" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/type_content1_img3.jpg" alt="type" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">金属プレートの表面を薬品で溶かして溝を作り、凹ませた部分に色を入れてデザインを表現する製法です。プレス製法に比べて、柔らかい線で細密なデザインが表現でき、線の幅や表情も強弱をつけることができます。金属の質感を生かしたデザインのオリジナルピンバッジを作りたい方にお勧めです。</div>
				<div class="sub-link"><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/glossary#glossary1">エッチングについてもっと知りたい</a></div>
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->

	<div id="type2" class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">真鍮プレス</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/type_content2_img1.jpg" alt="type" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/type_content2_img2.jpg" alt="type" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/type_content2_img3.jpg" alt="type" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">
					<p>金属プレートに圧力をかけて金型を押し付けることにより、デザインを刻印します。溝をくっきりと深くつけることが可能で、文字などの細かいデザインでも非常にはっきりキレイに仕上がります。重厚な質感を感じさせるため、社章などのオリジナルピンバッジにはこのプレス製法がよく選ばれています。</p>
					<p>金属プレートに圧力をかけて金型を押し付けることにより、デザインを刻印します。溝をくっきりと深くつけることが可能で、文字などの細かいデザインでも非常にはっきりキレイに仕上がります。重厚な質感を感じさせるため、社章などのオリジナルピンバッジにはこのプレス製法がよく選ばれています。</p>
				</div>
				<div class="sub-link"><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/glossary#glossary2">プレスについてもっと知りたい</a></div>
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">ラバーキャスト</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/type_content3_img1.jpg" alt="type" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/type_content3_img2.jpg" alt="type" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/type_content3_img3.jpg" alt="type" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">デザインに基づいてラバー型を作り、そこにPVC樹脂を流し込んで作ります。文字やロゴ、イラスト、キャラクターなどを立体的に表現でき、アピール度抜群のオリジナルピンバッジが制作できます。ラバー型なので、亜鉛キャストより型代が安く、小ロットからの生産が可能です。</div>
				<div class="sub-link"><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/glossary#glossary3">ラバーキャストについてもっと知りたい</a></div>
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">亜鉛キャスト</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/type_content4_img1.jpg" alt="type" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/type_content4_img2.jpg" alt="type" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/type_content4_img3.jpg" alt="type" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">立体の金型に亜鉛合金を流し込んで成形します。文字やロゴ、イラスト、キャラクターなどを、細かなデザインでも細部まで立体的に表現したオリジナルピンバッジが制作できます。ラバーキャストよりも型代は多少高くなりますが、仕上がりの美しさや奥行きの深さは違った満足感を得られます。</div>
				<div class="sub-link"><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/glossary#glossary4">亜鉛キャストについてもっと知りたい</a></div>
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="step-content-bg clearfix">			
				<a class="step-info-left" href="<?php bloginfo('url'); ?>/">				
					<i class="fa fa-angle-left"></i>													
					<span class="text-type">トップページにもどる</span>
				</a>		
				<a class="step-info-right" href="<?php bloginfo('url'); ?>/shape">
					<span class="step-left">Step2</span>				
					<span class="pl10">形状を選ぶ</span>
					<i class="fa fa-angle-right"></i>
				</a>		
		</div><!-- end step -->
	</div><!-- end primary-row -->
<?php get_footer(); ?>