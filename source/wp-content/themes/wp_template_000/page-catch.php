<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="catch-top-content">
		<h1 class="sub-top-title"><span class="sub-title-step">Step5</span>留め具を選ぶ</h1>
	</div>
	<div class="sub-top-text">
		<p>オリジナルピンバッジは、どこかに着けられてことその効果を発揮します。留め具の種類によって、着脱のしやすさや仕上がりの印象が変わります。制作目的や用途に合わせてお好きな留め具をお選びください。</p>
		<p>また、ピンバッジ用の留め具だけでなく、ネクタイピン用の留め具もご用意しております。</p>
		<p>サイトに掲載されている留め具の種類はほんの一部ですので、下記に掲載以外の留め具をご要望の方はスタッフまでお気軽にご相談ください。</p>	
	</div>
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">蝶タック</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content1_img1.jpg" alt="catch" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content1_img2.jpg" alt="catch" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content1_img3.jpg" alt="catch" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">バタフライクラッチとも呼ばれており、ピンバッジの留め具として最も一般的な形状です。着脱が非常に簡単で、比較的安価で制作できます。</div>
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->

	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">タイタック</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content2_img1.jpg" alt="catch" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content2_img2.jpg" alt="catch" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content2_img3.jpg" alt="catch" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">蝶タックより安定感があり、しっかりと留められます。重みや高級感があるので、一般的に社章などによく使われている留め具です。</div>
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">ロータリーネジ</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content3_img1.jpg" alt="catch" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content3_img2.jpg" alt="catch" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content3_img3.jpg" alt="catch" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">ネジでくるくると回して留めるので、外れにくくしっかりとした作りです。男性用のスーツの衿に取り付けることができます。</div>
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">ネクタイピン</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content4_img1.jpg" alt="catch" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content4_img2.jpg" alt="catch" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/catch_content4_img3.jpg" alt="catch" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">留め具としてネクタイピンの選択も可能です。ピンバッジの留め具を変えるだけで、オリジナルネクタイピンとして制作することができます。</div>
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="step-content-bg clearfix">			
				<a class="step-info-left" href="<?php bloginfo('url'); ?>/print"> 
					<i class="fa fa-angle-left"></i>
					<span class="step-right">Step4</span>
					着彩方法を選ぶ
				</a>	
			
				<a class="step-info-right text-center" href="<?php bloginfo('url'); ?>/estmation">					
					お見積もり
					<i class="fa fa-angle-right"></i>
				</a>		
		</div><!-- end step -->
	</div><!-- end primary-row -->
<?php get_footer(); ?>