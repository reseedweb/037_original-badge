<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="flow-top-content">
		<h1 class="top-content-title">ご注文の流れ</h1>
	</div>
	<p class="flow-top-text">ピンバッジのお問い合わせ・お見積もり依頼から納品までの流れをご案内いたします。</p>
	<div class="primary-row clearfix">
		<h2 class="h2-title">1. お問い合わせ・ヒアリング</h2>
		<div class="message-left message-342 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="flow" />
			</div>
			<div class="text">
				<p>まずは当サイトのお問い合わせ・お見積もりフォームもしくはお電話からお気軽にご相談ください。</p>
				<p>ピンバッジはそのデザインや製作方法・メッキの種類・サイズなどによって価格も変動します。</p>
				<p class="pt20">お問い合わせの際には、ご希望のオリジナルピンバッジのデザイン・数量・納期などをなるべく詳しくお伝えいただけると、お見積もりの提出がスムーズです。</p>
			</div>
		</div><!-- end message-342-->
		<div class="flow-content1 clearfix">
			<p>ピンバッジのデザインがまだお決まりでない場合は、当社の製作実績のなかからイメージに近いものをご指定いただくか、「こんな感じのピンバッジを作りたい」「このロゴマークを使いたい」など参考のイメージやご希望をお伝えください。より具体的なお見積もり金額をご提示できます。</p>
			<p class="pt20">どのようなものを作ろうか迷っている方は、ぜひ弊社のスタッフまでお気軽にご相談ください。</p>
			<p>ご希望のイメージや用途に合わせてご提案いたします。</p>
		</div><!-- end flow-content1  -->
		<div class="top-part-contact clearfix">		
			<div class="top-part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="flow" />
				</a>
			</div>
		</div><!-- /top-part-contact -->
		<div class="flow-content-arrow clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />
		</div><!-- end flow-content-arrow -->
	</div><!-- end primary-row -->

	<div id="flow-content-info" class="clearfix">
		<h2 class="h2-title">2. お見積提出</h2>
		<div class="message-left message-342 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="flow" />
			</div>
			<div class="text">
				<p>お問い合わせからヒアリングさせていただいた内容をもとに、お見積もりを提出いたします。</p>
				<p>もし数量や仕様に変更がある場合は、その旨をスタッフまでお伝えください。改めて再見積もりさせていただきます。</p>
				<p>ここまで費用は一切かかりませんのでご安心ください。</p>
				<p>ご納得のいくまでお客様のご要望をお聞かせください。</p>
			</div>
		</div><!-- end message-342-->
		<div class="flow-content-arrow clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />
		</div><!-- end flow-content-arrow -->
	</div><!-- end flow-content-info -->
	
	<div id="flow-content-info" class="clearfix">
		<h2 class="h2-title">3. ご注文・データ入稿</h2>
		<div class="message-left message-342 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="flow" />
			</div>
			<div class="text">
				<p>お見積もりとピンバッジの仕様に納得いただけましたら、正式にご注文となります。</p>
				<p>お客様自身でデザインデータを作成し、ご入稿ください。</p>
				<p>もし、ご自身でデザインデータを製作することが難しい場合は、ご希望のイメージから弊社でデータを作成することも可能です。内容によっては別途費用が発生する場合もございます。</p>
				<p>あらかじめご了承ください。</p>
				<p>データ入稿に関して詳しくは<a href="<?php bloginfo('url'); ?>/datas"><span class="flow-link">「デザイン入稿について」</span></a>のページを参照ください。</p>
			</div>
		</div><!-- end message-342-->
		<div class="flow-content-arrow clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />
		</div><!-- end flow-content-arrow -->
	</div><!-- end flow-content-info -->
	
	<div id="flow-content-info" class="clearfix">
		<h2 class="h2-title">4. 製造</h2>
		<div class="message-left message-342 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="flow" />
			</div>
			<div class="text">
				<p>ご注文の仕様とデザインデータをもとに、オリジナルピンバッジの製造に入ります。デザインや数量によりますが、データ校了から通常３～４週間で商品発送が可能です。</p>
				<p>デザイン入稿後にご注文をキャンセルされる場合は、それまでにかかった費用に基づいてキャンセル費をご請求させていただきますので、ご了承ください。</p>
				<p class="pt20">大ロットの発注、もしくはお客様からのご要望があれば、サンプル校正を製作することも可能です。</p>
				<p>サンプルはデザインにもよりますが、通常２週間～３週間で製作できます。実物と同じサンプル品を手にとってご確認	いただき、OKであればそのまま量産に入ります。</p>				
			</div>
		</div><!-- end message-342-->
		<div class="flow-content-arrow clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />
		</div><!-- end flow-content-arrow -->
	</div><!-- end flow-content-info -->
	
	<div id="flow-content-info" class="mb30 clearfix">
		<h2 class="h2-title">5. 検品・納品</h2>
		<div class="message-left message-342 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="flow" />
			</div>
			<div class="text">
				<p>検品後、宅配便にてご指定地までお届けいたします。</p>
				<p>複数箇所への分納も可能です。必要があればスタッフまで<br />事前にご相談ください。</p>
				<p>オリジナルピンバッジが手元に届きましたら、ぜひその手に<br />とって仕上がりをご確認ください。</p>
				<p>弊社は品質を第一に製造・検品に勤めておりますが、万が一<br />ピンバッジの仕様に不良があった場合は、直ちに再製作・<br />もしくは製作費用を返金いたします。</p>
				<p>担当スタッフまでご連絡ください。</p>				
			</div>
		</div><!-- end message-342-->		
	</div><!-- end flow-content-info -->
		
<?php get_footer(); ?>