<?php get_header(); ?>
<div class="primary-row-row clearfix"><!-- begin sidebar-row -->
	<div class="top-main1">		
		<div class="top-main1-btn">
			<a href="<?php bloginfo('url'); ?>/about">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img1_btn.jpg" alt="top" />
			</a>
		</div>
	</div><!-- /top-main1 -->
</div><!-- end primary-row -row -->

<?php get_template_part('part','topbox1'); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->
	<a href="<?php bloginfo('url'); ?>/price">
		<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img2.jpg" alt="top" />	
	</a>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<a href="<?php bloginfo('url'); ?>/service">
			<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img3.jpg" alt="top" />	
		</a>
	</div><!-- /primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="top-main2-content">
			<div class="top-main2-img1">
				<a href="<?php bloginfo('url'); ?>/glossary">				
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img4.jpg" alt="top" />	
				</a>
			</div>
			<div class="top-main2-img2">
				<a href="<?php bloginfo('url'); ?>/datas">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_main_img5.jpg" alt="top" />	
				</a>
			</div>
		</div>
	</div><!-- /primary-row k-->
</div><!-- end primary-row -->

<?php get_template_part('part','topslider'); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-video clearfix">
		<div class="top-video1">
			<iframe width="356" height="202" src="https://www.youtube.com/embed/Tv3QotZ9_kk" frameborder="0" allowfullscreen></iframe>
		</div>
		<div class="top-video2">
			<iframe width="356" height="202" src="https://www.youtube.com/embed/Tv3QotZ9_kk" frameborder="0" allowfullscreen></iframe>
		</div>
	</div><!-- /top-video -->
</div><!-- end primary-row -->

<?php get_template_part('part','topbox2'); ?>

<?php get_template_part('part','contact'); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-title">オリジナルピンバッジ工房からのご挨拶</h2>
	<div class="ln15em mt20">
		<p>初めまして。オリジナルピンバッジ工房のサイトにお越しいただきましてありがとうございます。</p>
		<p>私たちは江戸時代より伝統で伝わるエッチング、プレス、キャストなどの技術を用いて、お客様のデザインを精密に再現するピンバッジ製造を誇りにしています。</p>
		<p class="pt20">またピンバッジ以外にも七宝焼・金さし・象牙などの特殊な技術を、文具・ブローチ・ストラップ・アクセサリー・タイピン・バッチ・ボタン・バックルなどに応用しました。</p>
		<p>小ロット多品種のオリジナル商品に専念し、現在では取扱アイテムは数万点以上にのぼります。</p>
		<p>ただデザインや希望を精密に再現するだけでなく、商品を安価に生産し提供するため徹底した合理化にも尽力しております。</p>
		<p>これからは、時代の変化にあわせて「環境」「健康」「安全」をキーワードに邁進してまいります。</p>
		<p class="pt20">精密で品質の高いオリジナルピンバッジを製作したい方、ぜひ私たちにご相談ください。</p>
		<p>初めての方にもメーカーの立場から丁寧にご説明いたします。</p>
		<p>今後ともオリジナルピンバッジ工房をよろしくお願いいたします。</p>
	</div>	
</div><!-- end primary-row -->

<?php get_footer(); ?>