<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<div class="side-con-bg">
		<div class="side-con-text">
			<p class="side-con-text1">はじめての方でも</p>
			<p class="side-con-text2">選んで簡単！</p>
		</div>
		<div class="side-con-btn">
			<a href="<?php bloginfo('url'); ?>/estimation">
				<img src="<?php bloginfo('template_url'); ?>/img/common/side_con_btn.jpg" alt="sidebar" />
			</a>
		</div>
	</div><!-- /side-con-bg -->		
</div><!-- end sidebar-row -->


<?php get_template_part('part','sideMnu')?>	

<?php get_template_part('part','sideNavi')?>	


<div class="sidebar-row clearfix">
	<div class="sideBlog">
		<h4 class="sideBlog-title">最新の投稿</h4>
		<ul>
			<?php query_posts("posts_per_page=5"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
			<li>
			<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
			</li>
			<?php endwhile;endif; ?>
		</ul> 
	</div>
	<div class="sideBlog">
	    <h4 class="sideBlog-title">最新の投稿</h4>
	      <ul class="category">
			<?php wp_list_categories('sort_column=name&optioncount=0&hierarchical=1&title_li='); ?>
	      </ul>                             	
	</div>
	<div class="sideBlog">
      <h4 class="sideBlog-title">最新の投稿</h4>
      <ul><?php wp_get_archives('type=monthly&limit=12'); ?></ul>                               	
	</div>
</div>
