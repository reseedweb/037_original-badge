<?php get_header(); ?> 
	<?php get_template_part('part','breadcrumb');?>
	<!--<div class="example-top-content">
		<h1 class="sub-top-title"><span class="sub-title-step">Step5</span>留め具を選ぶ</h1>
	</div> -->
    <?php query_posts(array('posts_per_page' => 5, 'paged' => get_query_var('paged') )); ?>
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->
        <div class="primary-row clearfix">            
            <h2 class="h2-title"><?php the_title(); ?></span></h2>            
            <div class="post-row-content clearfix">                
                <div class="post-row-meta">
                    <i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
					<i class="fa fa-tags"></i><span class="blog-catelogy"><?php the_category(' , ', get_the_id()); ?></span>
                    <i class="fa fa-user"></i><span class="blog-author"><?php the_author_link(); ?></span>
                </div>
                <div class="post-row-description"><?php the_content(); ?></div>                    
            </div>
        </div><!-- end primary-row -->
        <?php endwhile; ?>    
        <div class="primary-row">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>     
<?php get_template_part('part','contact') ;?>
<?php get_footer(); ?>