<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="color-top-content">
		<h1 class="sub-top-title"><span class="sub-title-step">Step3</span>メッキの色を選ぶ</h1>
	</div>
	<p class="sub-top-text">仕上がりのメッキの色を選びます。オリジナルピンバッジの仕上がりの美しさや印象を大きく左右するので、特にこだわれる方も多い工程です。このページには、豊富なメッキの種類のなかから人気が高く一般的なカラーを選び掲載しています。当社で制作可能なメッキカラーのほんの一部でもあります。もし他の色がご覧になられたい場合は、是非お気軽にスタッフまでお問い合わせください。</p>
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">メッキの色</h2>
		<div class="color-content clearfix"><!-- begin color-content -->
			<div class="color-info">
				<div class="color-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/color_content_img1.jpg" alt="color" />
				</div>			
				<h3 class="color-title">ゴールド</h3>
				<div class="color-text">
					<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>				
				</div>				
			</div><!--/color-info -->	

			<div class="color-info">
				<div class="color-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/color_content_img2.jpg" alt="color" />
				</div>			
				<h3 class="color-title">シルバー</h3>
				<div class="color-text">
					<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>				
				</div>				
			</div><!--/color-info -->
			
			<div class="color-info">
				<div class="color-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/color_content_img3.jpg" alt="color" />
				</div>			
				<h3 class="color-title">ゴールド＆シルバー</h3>
				<div class="color-text">
					<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>				
				</div>				
			</div><!--/color-info -->
		</div><!-- end color-content -->
		
		<div class="color-content clearfix"><!-- begin color-content -->
			<div class="color-info">
				<div class="color-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/color_content_img4.jpg" alt="color" />
				</div>			
				<h3 class="color-title">アンティークゴールド</h3>
				<div class="color-text">
					<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>				
				</div>				
			</div><!--/color-info -->	

			<div class="color-info">
				<div class="color-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/color_content_img5.jpg" alt="color" />
				</div>			
				<h3 class="color-title">アンティークシルバー</h3>
				<div class="color-text">
					<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>				
				</div>				
			</div><!--/color-info -->			
		</div><!-- end color-content -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="step-content-bg clearfix">			
				<a class="step-info-left" href="<?php bloginfo('url'); ?>/shape">
					<i class="fa fa-angle-left"></i>
					<span class="step-right">Step2</span>
					形状を選ぶ
				</a>		
			
				<a  class="step-info-right" href="<?php bloginfo('url'); ?>/print">					
					<span class="step-left">Step4</span>
					着彩方法を選ぶ
					<i class="fa fa-angle-right"></i>
				</a>	
		</div><!-- end step -->
	</div><!-- end primary-row -->
		
<?php get_footer(); ?>