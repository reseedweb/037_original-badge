<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-title">オリジナルピンバッジをタイプから選ぶ</h2>
	<div class="top-box1-content clearfix"><!-- begin top-box1-content -->
		<div class="top-box1-info">
			<div class="top-box1-img">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_img1.jpg" alt="top" />
			</div>
			<div class="top-box1-icon">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_icon1.png" alt="top" />
			</div>
			<h3 class="top-box1-title">真鍮エッチング</h3>
			<div class="top-box1-text">
				<p>金属の表面を薬剤で溶かして絵柄を表現します。</p>
				<p>ペンで書くのと同じように細かなデザインの再現が可能です。</p>
			</div>
			<div class="top-box1-btn">
				<a href="<?php bloginfo('url'); ?>/shape">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_btn.jpg" alt="top" />
				</a>				
			</div>
		</div><!--/top-box1-info -->
		
		<div class="top-box1-info">
			<div class="top-box1-img">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_img2.jpg" alt="top" />
			</div>
			<div class="top-box1-icon">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_icon2.png" alt="top" />
			</div>
			<h3 class="top-box1-title">真鍮プレス</h3>
			<div class="top-box1-text">
				<p>絵柄をプレスして凹凸を作ります。線がくっきり仕上がるので、シンプルなデザインを表現したい場合に向いています。</p>				
			</div>
			<div class="top-box1-btn">
				<a href="<?php bloginfo('url'); ?>/shape">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_btn.jpg" alt="top" />
				</a>				
			</div>
		</div><!--/top-box1-info -->
		
		<div class="top-box1-info">
			<div class="top-box1-img">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_img3.jpg" alt="top" />
			</div>
			<div class="top-box1-icon">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_icon3.png" alt="top" />
			</div>
			<h3 class="top-box1-title">ラバーキャスト</h3>
			<div class="top-box1-text">
				<p>ゴム型に金属を流し込んで鋳造します。立体感のあるピンバッジを小ロットでも安価に生産できることが魅力です。</p>				
			</div>
			<div class="top-box1-btn">
				<a href="<?php bloginfo('url'); ?>/shape">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_btn.jpg" alt="top" />
				</a>				
			</div>
		</div><!--/top-box1-info -->
		
		<div class="top-box1-info">
			<div class="top-box1-img">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_img4.jpg" alt="top" />
			</div>
			<div class="top-box1-icon">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_icon4.png" alt="top" />
			</div>
			<h3 class="top-box1-title">亜鉛キャスト</h3>
			<div class="top-box1-text">
				<p>亜鉛型に金属を流し込んで鋳造します。複雑な立体物や細かなデザインまで精密にピンバッジに再現できます。</p>				
			</div>
			<div class="top-box1-btn">
				<a href="<?php bloginfo('url'); ?>/shape">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_box1_btn.jpg" alt="top" />
				</a>				
			</div>
		</div><!--/top-box1-info -->
	</div><!-- end top-box1-content -->
</div><!-- end primary-row -->