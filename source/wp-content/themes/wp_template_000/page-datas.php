<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="datas-top-content">
		<h1 class="sub-top-title">データ入稿について</h1>
	</div>
	<div class="sub-top-text">
		<p>お見積もりとピンバッジの仕様に納得いただけましたら、正式にご注文となります。</p>
		<p>お客様自身でデザインデータを作成し、ご入稿ください。もし、ご自身でデザインデータを製作することが難しい場合は、ご希望のイメージから弊社でデータを作成することも可能です。</p>
		<p>内容によっては別途費用が発生する場合もございます。あらかじめご了承ください。</p>
	</div>	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">入稿に関しての注意点</h2>		
		<div class="datas-top-content1 clearfix">
			<div class="datas-top-content1-text">
				<h3 class="datas-top-content1-title">デザイン入稿の前に下記注意点を必ずお読みください。</h3>
				<div class="datas-info1-text">
					<p>入稿データに不備がありますと、オリジナルピンバッジの</p>
					<p>製造が開始できず納期が遅れる原因にもなりかねません。</p>
					<p>入稿に関するご不明点がありましたら、スタッフまでお問い合わせください。</p>
				</div>				
			</div>
			<div class="datas-top-content1-img"><img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_img1.jpg" alt="datas" /></div>
		</div><!-- end datas-content1  -->
		<ul class="datas-list clearfix">
			<li class="datas-list-bg1">デザインの原稿は基本的に<span class="datas-clr">Illustrator(ai/eps)形式</span>でお送りください。</li>
			<li class="datas-list-bg2">Illustratorをお持ちでない方に限って、PDFのデータでお送りください。</li>
			<li class="datas-list-bg1">Illustratorのバージョンは<span class="datas-clr">CS6まで</span>対応しております。</li>
			<li class="datas-list-bg2"><span class="datas-clr">カラーモードはCMYK</span>を選択してください。</li>
			<li class="datas-list-bg1"><span class="datas-clr">解像度は350dpi以上</span>にしてください。解像度が低いと画像が荒くなる可能性があります。</li>
			<li class="datas-list-bg2">Illustratorに<span class="datas-clr">配置した画像は全て「埋め込み」</span>してください。</li>
			<li class="datas-list-bg1">フォントは<span class="datas-clr">必ず「文字のアウトライン化」</span>をしてください。</li>
			<li class="datas-list-bg2">データの<span class="datas-clr">作成は、原寸にて</span>お願いします。</li>
			<li class="datas-list-bg1">添付ファイルはできるだけ圧縮して、容量は最大3MBくらいにして頂けるようにお願いします。</li>
		</ul>
		
		<div class="datas-top-content2 clearfix">
			<div class="datas-top-content2-info1">
				<h3 class="datas-top-content2-title">既成型のビンバッジを制作する場合</h3>
				<div class="datas-top-content2-img"><img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_img2.jpg" alt="datas" /></div>
				<div class="datas-top-content2-text">
					<p>既製型の場合は下記「入稿用テンプレートダウンロード」から、ご注文の形状とサイズに合うテンプレート（ai）をダウンロードしてください。</p>
					<p>テンプレート枠内にオリジナルピンバッジのデザインをお願いします。</p>
				</div>
			</div><!-- end datas-content2-info1 -->
			
			<div class="datas-top-content2-info2">
				<h3 class="datas-top-content2-title">オリジナル形状のピンバッジを制作する場合</h3>
				<div class="datas-top-content2-img"><img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_img3.jpg" alt="datas" /></div>
				<div class="datas-top-content2-text">
					<p>お客様自身でIllustratorにて入稿用のデザインデータを作成ください。ご自身でデザインデータを作成することが難しい場合は、イメージ図やご要望をお聞きし弊社でデータを作成することも可能です。（別途費用）</p>					
				</div>
			</div><!-- end datas-content2-info2 -->
		</div><!-- end datas-content2 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row --> 
		<h2 class="h2-title">入稿用テンプレートダウンロード</h2>		
		<h3 class="datas-content-title mt30">丸型</h3>
		<div class="datas-content clearfix"><!-- begin datas-content -->
			<div class="datas-info">
				<h4 class="datas-title">13mm</h4>
				<div class="datas-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content1_img1.jpg" alt="datas" />
				</div>							
				<div class="datas-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_download.jpg" alt="datas" />
				</div>				
			</div><!--/datas-info -->	

			<div class="datas-info">
				<h4 class="datas-title">15mm</h4>
				<div class="datas-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content1_img2.jpg" alt="datas" />
				</div>							
				<div class="datas-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_download.jpg" alt="datas" />
				</div>				
			</div><!--/datas-info -->
			
			<div class="datas-info">
				<h4 class="datas-title">18mm</h4>
				<div class="datas-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content1_img3.jpg" alt="datas" />
				</div>							
				<div class="datas-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_download.jpg" alt="datas" />
				</div>				
			</div><!--/datas-info -->
			
			<div class="datas-info">
				<h4 class="datas-title">20mm</h4>
				<div class="datas-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content1_img4.jpg" alt="datas" />
				</div>							
				<div class="datas-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_download.jpg" alt="datas" />
				</div>				
			</div><!--/datas-info -->
		</div><!-- end datas-content -->		
	</div><!-- end primary-row -->

	<div class="primary-row clearfix"><!-- begin primary-row --> 		
		<div class="datas-content-left">
			<h3 class="datas-content-title mt30">正方形</h3>
			<div class="datas-content2 clearfix"><!-- begin datas-content -->
				<div class="datas-info2">
					<h4 class="datas-title">13mm</h4>
					<div class="datas-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content2_img1.jpg" alt="datas" />
					</div>							
					<div class="datas-btn">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_download.jpg" alt="datas" />
					</div>				
				</div><!--/datas-info2 -->	

				<div class="datas-info2">
					<h4 class="datas-title">15mm</h4>
					<div class="datas-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content2_img2.jpg" alt="datas" />
					</div>							
					<div class="datas-btn">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_download.jpg" alt="datas" />
					</div>				
				</div><!--/datas-info2 -->
				
				<div class="datas-info2">
					<h4 class="datas-title">18mm</h4>
					<div class="datas-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content2_img3.jpg" alt="datas" />
					</div>							
					<div class="datas-btn">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_download.jpg" alt="datas" />
					</div>				
				</div><!--/datas-info2 -->			
			</div><!-- end datas-content -->
		</div><!-- end datas-content-left -->
		
		<div class="datas-content-right">	
			<h3 class="datas-content-title mt30">小判型</h3>
			<div class="datas-content3 clearfix"><!-- begin datas-content -->
				<div class="datas-info3">
					<h4 class="datas-title">15mm</h4>
					<div class="datas-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content3_img1.jpg" alt="datas" />
					</div>							
					<div class="datas-btn">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_download.jpg" alt="datas" />
					</div>				
				</div><!--/datas-info3 -->	

				<div class="datas-info3">
					<h4 class="datas-title">20mm</h4>
					<div class="datas-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content3_img2.jpg" alt="datas" />
					</div>							
					<div class="datas-btn">
						<img src="<?php bloginfo('template_url'); ?>/img/content/datas_content_download.jpg" alt="datas" />
					</div>				
				</div><!--/datas-info3 -->			
			</div><!-- end datas-content -->
		</div><!-- end datas-content-left -->		
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">入稿はこちらから</h2>	
		<p class="datas-content4-top-text">作成したデザインデータやイメージ図は下記メールアドレスに添付してご入稿をお願いします。</p>
		<div class="datas-content4-email"><a href="mailto:data@rebecca-g.com">data@original-badge.com</a></div>
		<div class="datas-content4 clearfix">
			<div class="datas-content4-left clearfix">
				<h3 class="datas-content4-title">容量の大きいファイルの場合</h3>
				<p class="datas-content4-text1">4MBを超える容量の大きいデータは極力データを圧縮<br />いただくか、下記データ転送サービスをご利用ください。</p>
				<p class="datas-content4-links">● 宅ファイル便（300MBまで無料）<br /><span class="datas-links-a"><a href="http://www.filesend.to/">http://www.filesend.to/</a></span></p>
				<p class="datas-content4-links">● 宅ファイル便（300MBまで無料）<br /><span class="datas-links-a"><a href="http://gigafile.nu/">http://www.filesend.to/</span></p>
			</div>
			<div class="datas-content4-right clearfix">
				<h3 class="datas-content4-title">郵送の場合</h3>
				<div class="datas-content4-text1">
					<p>デザインデータをCD-ROMもしくはUSBなどに収容し、<br />下記宛先まで郵送でお送りいただく事も可能です。</p>
					<p>※尚お送りいただいたCD-ROMなどの　ご返却出来かねますので、ご了承ください。</p>
				</div>
				<div class="datas-content-text2">
					<p>〒546-0003</p>
					<p class="pl10">大阪市東住吉区今川4丁目14番4号</p>
					<p class="pl10">オリジナルピンバッジ工房 宛</p>
				</div>
			</div>
		</div>
	</div><!-- end primary-row -->	
	
	<div class="primary-row mb40 clearfix"><!-- begin primary-row -->    
		<div class="top-part-contact clearfix">		
			<div class="top-part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="flow" />
				</a>
			</div>
		</div><!-- /top-part-contact -->
	</div><!-- end primary-row -->
<?php get_footer(); ?> 