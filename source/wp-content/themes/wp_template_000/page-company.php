<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">会社概要</h2>
		<table class="company_table mt20">
			<tr>
				<th>社名</th>
				<td>清水工業株式会社</td>
			</tr>
			<tr>
				<th>連絡先</th>
				<td>
					<p>本社<br />大阪市東住吉区今川4丁目14番4号<br />TEL：06-6702-2555　　FAX：06-6703-7678</p>
					<p class="pt20">東京店<br />東京都台東区台東1丁目18番5号<br />TEL：03-3835-2258　　FAX：03-3835-2290</p>
				</td>
			</tr>
			<tr>
				<th>創業</th>
				<td>昭和7年4月</td>
			</tr>
			<tr>
				<th>代表者</th>
				<td>清水 昭憲</td>
			</tr>
			<tr>
				<th>資本金</th>
				<td>2,000万円</td>
			</tr>
			<tr>
				<th>事業内容</th>
				<td>金属釦・服飾付属通全般（ピンバッジ、諸官庁制服釦、紳士服金属釦、婦人服金属釦、バッ<br />クル、ベルト、ステーショナリーグッズ、ブローチ、バッチ、靴・鞄の付属、学生服釦）</td>
			</tr>						
		</table>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title mb20">アクセス</h2>
		<div id="company_map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3283.0354038314863!2d135.5353917!3d34.62854559999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000de881c53e37d%3A0x991cb7f32bf36dce!2s4+Chome-14-4+Imagawa%2C+Higashisumiyoshi-ku%2C+%C5%8Csaka-shi%2C+%C5%8Csaka-fu+546-0003%2C+Japan!5e0!3m2!1sen!2s!4v1432177404445" width="760" height="300" frameborder="0" style="border:0"></iframe>
		</div>
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>