var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
$ = jQuery.noConflict();
$(document).ready(function(){
    //menu-toggle
    $('#menu-toggle ul a').each(function(){
        var ele = $(this);
        $(ele).prepend('<i class="fa fa-chevron-right mr10"></i>');
    })
    var screen_type = $('#screen_type').css('content');
    
    if(
        screen_type === 'lg' || screen_type === 'md' || screen_type === 'sm' // chrome
        ||
        screen_type === '"lg"' || screen_type === '"md"' || screen_type === '"sm"' //firefox
    ){
    //if( !isMobile.any() ){
        $('#menu-toggle').addClass('in');
        $('#menu-toggle').show();
    }
    // ie container width fix
    $('.container').css('width', CONTAINER_WIDTH);
    // hover white background
    $('a').hover(function(){                
        $(this).children('img').parent('a').addClass('img_bg_white');             
        if( ($(this).hasClass('on_off')) ){
            $(this).children('img').parent('a').removeClass('img_bg_white');             
        }
    },function(){
        $(this).removeClass('img_bg_white');
    });    
    $('a.on_off').hover(function(){
        var temp_src = $(this).children('img').attr('src');        
        $(this).children('img').attr('src',$(this).children('img').attr('data-hover') );
        $(this).children('img').attr('data-hover',temp_src);
    },function(){
        var temp_src = $(this).children('img').attr('src');
        $(this).children('img').attr('src',$(this).children('img').attr('data-hover') );
        $(this).children('img').attr('data-hover',temp_src);
    })    
    // sideMenu
    $('.sideMenu ul li').each(function(){           
        if($(this).attr('data-height') != 'undefined'){
            $(this).children('a').css('line-height',parseInt($(this).attr('data-height')) + 'px');
        }
    });

});

    