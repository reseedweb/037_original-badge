<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="datas-top-content">
		<h1 class="sub-top-title">はじめての方へ</h1>
	</div>
	<div class="sub-top-text">
		<p>オリジナルピンバッジ工房にお越しいただきましてありがとうございます。</p>
		<p>弊社では、はじめてピンバッジを作られるお客様にも選んでいただきやすいように、<br />ステップ形式のオーダー方法をご用意しております。</p>
		<p>オリジナルピンバッジ製作でお悩みのことがありましたら、弊社スタッフまでお気軽にお問い合わせください。</p>
	</div>	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">ピンバッジ制作をお考えの方へ</h2>		
		<div class="about-content-top clearfix">
			<h3 class="about-top-title">ピンバッジ制作のプロに相談してみませんか？</h3>
			<ul class="about-top-list">
				<li>どんなオリジナルピンバッジにしようか迷っている</li>
				<li>できるだけ費用を抑えて安く製作したい</li>
				<li>手元にあるロゴデータやイラストからピンバッジを製作したい</li>
				<li>イベントグッズや販促品として効果的なものを製作したい</li>
				<li>小ロットや短納期で依頼できるところを探している</li>
			</ul>
		</div><!-- end about-content-top  -->		
		<div class="about-content mb30 clearfix"><!-- begin about-content -->
			<div class="about-info">
				<div class="about-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/about_content1_img1.jpg" alt="about" />
				</div>			
				<h3 class="about-title">
					<span class="about-title-l">ピンバッジ仕様</span>
					<span class="about-title-s">に関するお悩み</span>
				</h3>
				<div class="about-text">
					<p>エッチング・プレス・キャストなどピンバッジの仕様は多種多様で、どれを選べばよいか迷ってしまうこともあるかと思います。ご要望のイメージや製作目的をお伝えいただければ、ピンバッジ製作経験の豊富なスタッフがお客様のオリジナルピンバッジ製作を企画からサポートいたします</p>				
				</div>				
			</div><!--/about-info -->	
			<div class="about-info">
				<div class="about-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/about_content1_img2.jpg" alt="about" />
				</div>			
				<h3 class="about-title">
					<span class="about-title-l">納期・ロット</span>
					<span class="about-title-s">に関するお悩み</span>
				</h3>
				<div class="about-text">
					<p>イベントでの使用などで納期が決まっていてお急ぎの案件に対しても、可能な限り短納期でのピンバッジ製作をお引き受けいたします。その場合は選択できる仕様やデザインが限られてくる可能性がございますので、まずはご相談ください。個人様や企業様からの小ロットのご依頼も最小１個から承っております。</p>				
				</div>				
			</div><!--/about-info -->	
			<div class="about-info">
				<div class="about-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/about_content1_img3.jpg" alt="about" />
				</div>			
				<h3 class="about-title">
					<span class="about-title-l">その他の</span>
					<span class="about-title-s">お悩み・ご相談</span>
				</h3>
				<div class="about-text">
					<p>その他・デザインデータに関するお悩みやピンバッジ以外のオリジナルグッズ製作のご相談も随時承っております。弊社は服飾のボタンや、ベルト、ステーショナリーグッズ、ブローチ、缶バッジなどの各種金属製品の製作も得意としております。せひお気軽にお問い合わせください。</p>				
				</div>				
			</div><!--/about-info -->	
		</div><!-- end about-content -->
		<div class="top-part-contact clearfix">		
			<div class="top-part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="flow" />
				</a>
		</div>
		</div><!-- /top-part-contact -->			
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->   
		<h2 class="h2-title">オリジナルピンバッジ工房が選ばれる理由</h2>
		<div class="about-content2 clearfix">
			<div class="image"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content2_img1.jpg" alt="about" /></div>
			<div class="text">
				<h3 class="about-content2-title">自社生産&加工で低価格を実現</h3>
				<div class="about-content2-text">
					<p>オリジナルピンバッジ工房では、型の作成からピンバッジ製造・加工までほとんどの作業を自社の工場で行っております。中間マージンが最低限に抑えられるので、企画会社様や商社様に比べオリジナルピンバッジを低価格でご提供することが可能です。</p>
					<p>またこれまでの製作実績から既製型を豊富にご用意しております。既製型での製作なら型代がかからず製作コストを抑えられます。価格などを特に気にされる場合は、「参考価格例」のページも参考にご覧ください。</p>
				</div>
			</div>
		</div><!-- end message-342 -->
		<div class="about-content2 clearfix">
			<div class="image"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content2_img2.jpg" alt="about" /></div>
			<div class="text">
				<h3 class="about-content2-title">メーカーならではの提案力</h3>
				<div class="about-content2-text">
					<p>ピンバッジの製法や素材の知識に不安をお持ちの方にも、ピンバッジメーカーならではの視点で丁寧にご説明・ご提案します。初めての方もご安心ください。デザイン・絵柄にあわせてより再現度の高い製法の提案や、コスト削減のご相談などにもお応えいたします。</p>
					<p>また来社いただけるお客様には、スタッフが直接担当し、豊富な製作事例を実際に手にとってご覧いただくことが可能です。特にメッキの色や仕上がりなどを気にされる方は是非サンプルをご覧ください。来社することが難しい場合は、サンプルを郵送することも可能です。（要相談）</p>					
				</div>
			</div>
		</div><!-- end message-342 -->
		<div class="about-content2 clearfix">
			<div class="image"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content2_img3.jpg" alt="about" /></div>
			<div class="text">
				<h3 class="about-content2-title">小ロット・短納期にも対応可</h3>
				<div class="about-content2-text">
					<p>小ロット製造から大ロットの大量生産まで柔軟に承ります。個人の方でもお気軽にご相談ください。納期をお急ぎの場合にもできる限り対応させていただきます。</p>
					<p>その場合は、仕様やデザインが限られてくる可能性があります。予めご了承ください。</p>
					<p>通常オリジナルピンバッジの製作には、約３週間～４週間の期間をいただきます。既製型を利用すれば型の製作工程を省き、納期を短縮することが可能です。</p>
				</div>
			</div>
		</div><!-- end message-342 -->
	</div><!-- end primary-row -->	
<?php get_footer(); ?> 