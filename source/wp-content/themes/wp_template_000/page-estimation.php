<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->   
		<h2 class="h2-title"><?php the_title(); ?></h2>
		<div class="contact-form">			
			<?php echo do_shortcode('[contact-form-7 id="327" title="お見積もり"]') ?>					
		</div>
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->   
		<div class="top-part-contact clearfix">		
			<div class="top-part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="top" />
				</a>
			</div>
		</div><!-- /top-part-contact -->	
	</div><!-- end primary-row -->	
<?php get_footer(); ?>