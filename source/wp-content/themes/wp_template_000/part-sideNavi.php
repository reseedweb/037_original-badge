
<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<ul class="sideNavi-content">		
		<li><a href="<?php bloginfo('url'); ?>/">トップページ<i class="fa fa-play"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/about">初めての方へ<i class="fa fa-play"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/price">参考価格例<i class="fa fa-play"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/flow">ご注文の流れ<i class="fa fa-play"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/datas">データ入稿について<i class="fa fa-play"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/factory">工場の紹介<i class="fa fa-play"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/faq">よくある質問<i class="fa fa-play"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/company">会社概要<i class="fa fa-play"></i></a></li>
		<li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ<i class="fa fa-play"></i></a></li>
	</ul>
</div><!-- end sidebar-row -->