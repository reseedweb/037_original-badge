<?php get_header(); ?>
<div class="primary-row clearfix">	
	<?php get_template_part('part','breadcrumb');?>
	<!--<div class="example-top-content">
		<h1 class="sub-top-title"><span class="sub-title-step">Step5</span>留め具を選ぶ</h1>
	</div> -->
	<div class="example-content clearfix">		
	<?php	
	$example_posts = get_posts( 	array(
	    'post_type'=> 'example',			   
	    'paged' => get_query_var('paged'),
	));
	$i = 0;
	foreach($example_posts as $example_post):
	?>	    
        <div class="example-content-info">        
			<h2 class="h2-title"><?php echo $example_post->post_title; ?></h2>
			<div class="message-left message-340 clearfix">
				<div class="image">
					<a href="<?php echo get_permalink($example_post->ID ); ?>/">
						<?php echo get_the_post_thumbnail($example_post->ID,'example'); ?>
					</a>
				</div>
				<div class="text">
					<div class="example-ctf-table">					
						<table>		
							<tr>
								<th>箱形状</th>
								<td><?php echo get_field('ctf_01', $example_post->ID); ?></td>
							</tr>
							<tr>
								<th>厚み</th>
								<td><?php echo get_field('ctf_02', $example_post->ID); ?></td>
							</tr>
							<tr>
								<th>表面色</th>
								<td><?php echo get_field('ctf_03', $example_post->ID); ?></td>
							</tr>
							<tr>
								<th>印刷面</th>
								<td><?php echo get_field('ctf_04', $example_post->ID); ?></td>
							</tr>
																					
						</table>
					</div>
				</div>
			</div><!-- /message-340 -->
			<div class="content-text">
				<?php echo apply_filters('the_content', $example_post->post_content); ?>
			</div>
		</div><!-- end example-content-info -->    
	<?php endforeach; ?>
	</div><!-- example-content -->		
    <div class="primary-row text-center">
        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
    </div>
    <?php wp_reset_query(); ?>	
</div><!-- end primary-row -->

<?php get_template_part('part','contact') ;?>
<?php get_footer(); ?>