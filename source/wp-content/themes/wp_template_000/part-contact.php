<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-title">ご注文の流れ</h2>
	<div class="top-part-flow">
		<a href="<?php bloginfo('url'); ?>/flow">
			<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_flow.jpg" alt="top" />
		</a>
	</div>
    <div class="top-part-contact clearfix">		
		<div class="top-part-contact-btn">
			<a href="<?php bloginfo('url'); ?>/contact">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="top" />
			</a>
		</div>
	</div><!-- /top-part-contact -->
</div><!-- end primary-row -->