							<ul class="header-info2">
								<li>
									<a href="<?php bloginfo('url'); ?>/about">
										<span class="first-text">はじめての方へ</span>
										<span class="second-text">About</span>   
									</a>
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/flow">
										<span class="first-text">ご注文の流れ</span>
										<span class="second-text">Flow</span>   
									</a>
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/datas">
										<span class="first-text">データ入稿について</span>
										<span class="second-text">Datas</span>   
									</a>
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/faq">
										<span class="first-text">よくある質問</span>
										<span class="second-text">FAQ</span>   
									</a>
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/company">
										<span class="first-text">会社概要</span>
										<span class="second-text">Company</span>   
									</a>
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/factory">
										<span class="first-text">工場の紹介</span>
										<span class="second-text">factory</span>   
									</a>
								</li>
							</ul><!-- end header-info2 -->