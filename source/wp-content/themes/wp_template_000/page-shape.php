<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="shape-top-content">
		<h1 class="sub-top-title"><span class="sub-title-step">Step2</span>形状を選ぶ</h1>
	</div>
	<div class="sub-top-text">
		<p>ピンバッジの形状を決めます。オリジナル形状であれば、自由度が高くより個性的なピンバッジ制作が可能です。</p>
		<p>既製型と規定サイズのなかからお選びいただければ、型代がかからず費用を抑えて制作できます。</p>
		<p>どんなデザイン・形状にするか迷っている方は、まずは既製型での制作を考えられてみるのもお得でお勧めです。</p>
	</div>		
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">オリジナル形状・サイズ</h2>		
		<div class="message-left message-299 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_img.jpg" alt="shape" />
			</div>
			<div class="text">
				<p>お客様のデザイン形状に合わせて、オリジナルの形状・サイズを自由に設定できます。立体物やキャラクター・デザインの輪郭に合わせて型抜きをすることも可能です。個性的でオリジナル性の高いピンバッジで、イベントグッズや販促グッズとしての効果は抜群です。</p>
				<p>製法によってあまりにも小さいものや大きすぎるものは制作できない可能性がありますので、まずはこんなデザイン・サイズのピンバッジを作りたいというイメージをお伝えください。</p>
			</div>
		</div><!-- end message-299-->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">既成型から選ぶ</h2>		
		<div class="shape-text">	
			<p>弊社ではこれまでのピンバッジ制作の実績を生かし、よく選ばれる人気の形状に関して既製型を保有しています。</p>
			<p>ピンバッジ制作では一般的に成形するための型代が発生しますが、既製型のなかからお選びいただければ型代無料でお安く制作できます。</p>
			<p>既製型であっても、なかのデザインを自由に設定できるので、オリジナル性は損ないません。</p>
			<p>できるだけコストをかけずにオリジナルピンバッジを制作したい方にお勧めです。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h3 class="shape-content-title">丸型</h3>
		<div class="shape-content clearfix"><!-- begin shape-content -->
			<div class="shape-info">
				<h4 class="shape-title">13mm</h4>
				<div class="shape-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content1_img1.jpg" alt="shape" />
				</div>							
				<div class="shape-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_download.jpg" alt="shape" />
				</div>				
			</div><!--/shape-info -->	

			<div class="shape-info">
				<h4 class="shape-title">15mm</h4>
				<div class="shape-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content1_img2.jpg" alt="shape" />
				</div>							
				<div class="shape-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_download.jpg" alt="shape" />
				</div>				
			</div><!--/shape-info -->
			
			<div class="shape-info">
				<h4 class="shape-title">18mm</h4>
				<div class="shape-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content1_img3.jpg" alt="shape" />
				</div>							
				<div class="shape-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_download.jpg" alt="shape" />
				</div>				
			</div><!--/shape-info -->
			
			<div class="shape-info">
				<h4 class="shape-title">20mm</h4>
				<div class="shape-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content1_img4.jpg" alt="shape" />
				</div>							
				<div class="shape-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_download.jpg" alt="shape" />
				</div>				
			</div><!--/shape-info -->
		</div><!-- end shape-content -->		
		
		<div class="shape-content-text">	
			<p>最もオーソドックスで使い勝手の良い丸型のピンバッジです。多くのバッジが丸型なので、他のバッジと並べてつけてもよく馴染みます。サイズは13mm、15mm、18mm、20mmのなかからお選びいただけます。</p>
			<p class="shape-clr">※「ダウンロード」ボタンをクリックで、データ入稿用のテンプレート（ai形式）をダウンロードできます。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h3 class="shape-content-title">正方形</h3>
		<div class="shape-content clearfix"><!-- begin shape-content -->
			<div class="shape-info">
				<h4 class="shape-title">15mm</h4>
				<div class="shape-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content2_img1.jpg" alt="shape" />
				</div>							
				<div class="shape-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_download.jpg" alt="shape" />
				</div>				
			</div><!--/shape-info -->	

			<div class="shape-info">
				<h4 class="shape-title">18mm</h4>
				<div class="shape-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content2_img2.jpg" alt="shape" />
				</div>							
				<div class="shape-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_download.jpg" alt="shape" />
				</div>				
			</div><!--/shape-info -->
			
			<div class="shape-info">
				<h4 class="shape-title">20mm</h4>
				<div class="shape-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content2_img3.jpg" alt="shape" />
				</div>							
				<div class="shape-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_download.jpg" alt="shape" />
				</div>				
			</div><!--/shape-info -->		
		</div><!-- end shape-content -->		
		
		<div class="shape-content-text">	
			<p>イラストやロゴマークを入れるのちょうど良い正方形のピンバッジです。販促ノベルティグッズ、記念品など幅広い場面で活用されています。サイズは15mm、18mm、20mmのなかからお選びいただけます。</p>
			<p class="shape-clr">※「ダウンロード」ボタンをクリックで、データ入稿用のテンプレート（ai形式）をダウンロードできます。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h3 class="shape-content-title">小判型</h3>
		<div class="shape-content clearfix"><!-- begin shape-content -->
			<div class="shape-info">
				<h4 class="shape-title">15mm</h4>
				<div class="shape-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content3_img1.jpg" alt="shape" />
				</div>							
				<div class="shape-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_download.jpg" alt="shape" />
				</div>				
			</div><!--/shape-info -->	

			<div class="shape-info">
				<h4 class="shape-title">20mm</h4>
				<div class="shape-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content3_img2.jpg" alt="shape" />
				</div>							
				<div class="shape-btn">
					<img src="<?php bloginfo('template_url'); ?>/img/content/shape_content_download.jpg" alt="shape" />
				</div>				
			</div><!--/shape-info -->				
		</div><!-- end shape-content -->		
		
		<div class="shape-content-text">	
			<p>イラストや横長のロゴマークなどを入れるのちょうど良い小判形のピンバッジです。販促ノベルティグッズ、記念品などで活用されています。サイズは15mm、20mmのなかからお選びいただけます。</p>
			<p class="shape-clr">※「ダウンロード」ボタンをクリックで、データ入稿用のテンプレート（ai形式）をダウンロードできます。</p>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="step-content-bg clearfix">			
				<a class="step-info-left" href="<?php bloginfo('url'); ?>/type">
					<i class="fa fa-angle-left"></i>
					<span class="step-right">Step1</span>
					タイプを選ぶ
				</a>	
			
				<a class="step-info-right" href="<?php bloginfo('url'); ?>/color"> 
					<span class="step-left">Step3</span>
					メッキの色を選ぶ
					<i class="fa fa-angle-right"></i>
				</a>	
		</div><!-- end step -->
	</div><!-- end primary-row -->
	
<?php get_footer(); ?> 