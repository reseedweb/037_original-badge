<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="price-top-content">
		<h1 class="sub-top-title">参考価格例</h1>
	</div>
	<div class="sub-top-text">
		<p>オリジナルピンバッジは、その仕様・デザイン・数量などにより価格が大きく変わってまいります。</p>
		<p>ですので一概にピンバッジ製作にいくらかかるとは言えません。</p>
		<p>目安として、下記仕様でピンバッジを製作した場合の参考価格（１個あたりの単価）を下記に提示しております。</p>
		<p>この度お客様が製作を希望されているオリジナルピンバッジに関しては、随時個別にお見積もりをお出しいたします。</p>
		<p>まずはお電話か当サイトのお見積もりフォームよりご希望のピンバッジのイメージをお伝えください。</p>
		<p>また予算が決まっている方や、できるだけ安く製作したいとお考えの方はぜひスタッフまでご相談ください。</p>
		<p>ご希望にあわせてご提案いたします。</p>
	</div>		　　　　　　　　　　
　　　　　　
	<div class="mt20 clearfix"><!-- begin primary-row -->  
		<h2 class="h2-title">オリジナル形状の参考価格例</h2>
		<h3 class="price-title mt20">オリジナル形状 パターン１</h3>
		<div class="price-content1 clearfix">			
			<div class="price-img"><img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img1.jpg" alt="price" /></div>
			<div class="price-text">
				<h4 class="price-text-title"><i class="fa fa-circle-o"></i>下記の仕様の場合</h4>
				<table class="price-table1">
					<tbody>
						<tr>
							<td class="table1-clr1">タイプ</td>
							<td class="table1-clr2">0,000円</td>
							<td class="table1-clr1">形状</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
						<tr>
							<td class="table1-clr1">メッキの色</td>
							<td class="table1-clr2">0,000円</td>
							<td class="table1-clr1">着彩方法</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
						<tr>
							<td class="table1-clr1">留め具</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
					</tbody>
				</table><!-- end price-table1 -->
			</div>
		</div><!-- end price-content1 -->
				
		<div class="price-content2 clearfix">
			<div class="price-icon"><img src="<?php bloginfo('template_url'); ?>/img/content/price_content_icon.jpg" alt="price" /></div>
			<div class="price-p1">3000個で</div>
			<div class="price-p2">50,000<span class="unit">円</span></div>
			<div class="price-btn-small">
				<a href="<?php bloginfo('url'); ?>/estimation">
					<img src="<?php bloginfo('template_url'); ?>/img/content/price_btn_small.jpg" alt="price" />
				</a>				
			</div>			
		</div><!-- end price-content2 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->  		
		<h3 class="price-title">オリジナル形状 パターン２</h3>
		<div class="price-content1 clearfix">			
			<div class="price-img"><img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img2.jpg" alt="price" /></div>
			<div class="price-text">
				<h4 class="price-text-title"><i class="fa fa-circle-o"></i>下記の仕様の場合</h4>
				<table class="price-table1">
					<tbody>
						<tr>
							<td class="table1-clr1">タイプ</td>
							<td class="table1-clr2">0,000円</td>
							<td class="table1-clr1">形状</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
						<tr>
							<td class="table1-clr1">メッキの色</td>
							<td class="table1-clr2">0,000円</td>
							<td class="table1-clr1">着彩方法</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
						<tr>
							<td class="table1-clr1">留め具</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
					</tbody>
				</table><!-- end price-table1 -->
			</div>
		</div><!-- end price-content1 -->
				
		<div class="price-content2 clearfix">
			<div class="price-icon"><img src="<?php bloginfo('template_url'); ?>/img/content/price_content_icon.jpg" alt="price" /></div>
			<div class="price-p1">3000個で</div>
			<div class="price-p2">50,000<span class="unit">円</span></div>
			<div class="price-btn-small">
				<a href="<?php bloginfo('url'); ?>/estimation">
					<img src="<?php bloginfo('template_url'); ?>/img/content/price_btn_small.jpg" alt="price" />
				</a>				
			</div>			
		</div><!-- end price-content2 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row --> 	
		<div class="top-part-contact clearfix">		
			<div class="top-part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="flow" />
				</a>
			</div>
		</div><!-- /top-part-contact -->
	</div><!-- end primary-row -->		
	
	<div class="primary-row clearfix"><!-- begin primary-row --> 
		<h2 class="h2-title">既製型の参考価格例</h2>
		<h3 class="price-title mt20">丸型</h3>
		<div class="price-content1 clearfix">			
			<div class="price-img"><img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img3.jpg" alt="price" /></div>
			<div class="price-text">
				<h4 class="price-text-title"><i class="fa fa-circle-o"></i>下記の仕様の場合</h4>
				<table class="price-table1">
					<tbody>
						<tr>
							<td class="table1-clr1">タイプ</td>
							<td class="table1-clr2">0,000円</td>
							<td class="table1-clr1">形状</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
						<tr>
							<td class="table1-clr1">メッキの色</td>
							<td class="table1-clr2">0,000円</td>
							<td class="table1-clr1">着彩方法</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
						<tr>
							<td class="table1-clr1">留め具</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
					</tbody>
				</table><!-- end price-table1 -->
			</div>
		</div><!-- end price-content1 -->
				
		<table class="price-table2">
			<tbody>	
				<tr>
					<th class="price-table2-clr">サイズ</th>
					<th>300個</th>
					<th>500個</th>
					<th>1000個</th>
					<th>3000個</th>
					<th>5000個</th>
				</tr>
				<tr>
					<th>A</th>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
				</tr>
				<tr>
					<th>B</th>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
				</tr>
				<tr>
					<th>C</th>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
				</tr>				
			</tbody>		
		</table><!-- end price-table2 -->
		<div class="price-btn-large">
			<a href="<?php bloginfo('url'); ?>/estimation">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_btn_large.jpg" alt="price" />
			</a>
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row mb30 clearfix"><!-- begin primary-row --> 		
		<h3 class="price-title">正方形</h3>
		<div class="price-content1 clearfix">			
			<div class="price-img"><img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img4.jpg" alt="price" /></div>
			<div class="price-text">
				<h4 class="price-text-title"><i class="fa fa-circle-o"></i>下記の仕様の場合</h4>
				<table class="price-table1">
					<tbody>
						<tr>
							<td class="table1-clr1">タイプ</td>
							<td class="table1-clr2">0,000円</td>
							<td class="table1-clr1">形状</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
						<tr>
							<td class="table1-clr1">メッキの色</td>
							<td class="table1-clr2">0,000円</td>
							<td class="table1-clr1">着彩方法</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
						<tr>
							<td class="table1-clr1">留め具</td>
							<td class="table1-clr2">0,000円</td>							
						</tr>
					</tbody>
				</table><!-- end price-table1 -->
			</div>
		</div><!-- end price-content1 -->
				
		<table class="price-table2">
			<tbody>	
				<tr>
					<th class="price-table2-clr">サイズ</th>
					<th>300個</th>
					<th>500個</th>
					<th>1000個</th>
					<th>3000個</th>
					<th>5000個</th>
				</tr>
				<tr>
					<th>A</th>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
				</tr>
				<tr>
					<th>B</th>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
				</tr>
				<tr>
					<th>C</th>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
					<td>0,000円</td>
				</tr>				
			</tbody>		
		</table><!-- end price-table2 -->
		<div class="price-btn-large">
			<a href="<?php bloginfo('url'); ?>/estimation">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_btn_large.jpg" alt="price" />
			</a>
		</div>
	</div><!-- end primary-row -->
		
<?php get_footer(); ?> 