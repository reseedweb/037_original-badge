<div class="primary-row clearfix"><!-- begin primary-row -->    
	<div class="top-box2-content clearfix"><!-- begin top-box2-content -->
		<div class="top-box2-info">
			<div class="top-box2-img">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box2_img1.jpg" alt="top" />
			</div>			
			<h3 class="top-box2-title">自社生産&加工で低価格を実現</h3>
			<div class="top-box2-text">
				<p>オリジナルピンバッジ工房では、型の作成からピンバッジ製造・加工まで自社の工場で行っております。中間マージンが最低限に抑えられるので、企画・総合販促会社様に比べオリジナルピンバッジを低価格でご提供することが可能です。</p>				
			</div>
			<div class="top-box2-btn">
				<a href="<?php bloginfo('url'); ?>/about">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_box2_btn.jpg" alt="top" />
				</a>				
			</div>
		</div><!--/top-box2-info -->
		
		<div class="top-box2-info">
			<div class="top-box2-img">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box2_img2.jpg" alt="top" />
			</div>			
			<h3 class="top-box2-title">メーカーならではの提案力</h3>
			<div class="top-box2-text">
				<p>ピンバッジ製法や素材の知識に不安をお持ちの方にも、ピンバッジメーカーならではの視点で丁寧にご説明・ご提案します。初めての方もご安心ください。絵柄にあわせてより再現度の高い製法の提案や、コスト削減のご相談などにもお応えいたします。</p>				
			</div>
			<div class="top-box2-btn">
				<a href="<?php bloginfo('url'); ?>/about">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_box2_btn.jpg" alt="top" />
				</a>				
			</div>
		</div><!--/top-box2-info -->
		
		<div class="top-box2-info">
			<div class="top-box2-img">
				<img src="<?php bloginfo('template_url'); ?>/img/top/top_box2_img3.jpg" alt="top" />
			</div>			
			<h3 class="top-box2-title">小ロット・短納期にも対応可</h3>
			<div class="top-box2-text">
				<p>小ロット製造から大ロットの大量生産まで柔軟に承ります。個人の方でもお気軽にご相談ください。納期をお急ぎの場合にもできる限り対応させていただきます。その場合は、仕様や製法に限りがでる可能性があります。予めご了承ください。</p>				
			</div>
			<div class="top-box2-btn">
				<a href="<?php bloginfo('url'); ?>/about">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_box2_btn.jpg" alt="top" />
				</a>				
			</div>
		</div><!--/top-box2-info -->
	</div><!-- end top-box2-content -->
</div><!-- end primary-row -->