<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="print-top-content">
		<h1 class="sub-top-title"><span class="sub-title-step">Step4</span>着彩方法を選ぶ</h1>
	</div>
	<div class="sub-top-text">	
		<p>成形されたオリジナルピンバッジへの着彩方法をお選びいただけます。基本的には「色挿し」か「インクジェット印刷」をお選びいただき、さらにエポキシを（樹脂加工）するかしないかの選択になります。</p>
		<p>さらに詳しい印刷の方法や色挿しの塗料の種類などにご指定がある場合は、お見積もりの際スタッフまでお伝えください。<span class="print-clr">「ピンバッジ用語集」</span>にも着彩方法の解説を掲載しておりますので、そちらも参考にご覧ください。</p>
	</div>
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">色挿し</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content1_img1.jpg" alt="print" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/print_content1_img2.jpg" alt="print" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content1_img3.jpg" alt="print" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">ピンバッジの凹んでいる部分にラッカー塗料を流し込んで色付けします。色と色との境目には金属色の縁ができ、それぞれの色面が金属の縁に囲まれた仕上がりになります。縁と塗料を流し込んだ部分に多少の高低差ができ、表面の凸凹がわかる見た目になります。</div>
				<h3 class="sub-link-title"><i class="fa fa-circle-o"></i>この着彩が可能なピンバッジタイプ</h3>
				<ul class="sub-link-all clearfix">
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type1">真鍮プレス</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type2">真鍮エッチング</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ3">ラバーキャスト</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ4">亜鉛キャスト</a>
					</li>					
				</ul><!-- end sub-link-all -->
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">色挿し＋エポキシ（樹脂加工）</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content2_img1.jpg" alt="print" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/print_content2_img2.jpg" alt="print" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content2_img3.jpg" alt="print" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">色挿しで着彩をした後に、ピンバッジの表面に透明のエポキシ樹脂を乗せて仕上げます。表面がぷっくりと盛り上がった仕上がりになり、重厚感が出ます。より高級感のあるオリジナルピンバッジに仕上げたい方にお勧めです。</div>
				<h3 class="sub-link-title"><i class="fa fa-circle-o"></i>この着彩が可能なピンバッジタイプ</h3>
				<ul class="sub-link-all clearfix">
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type1">真鍮プレス</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type2">真鍮エッチング</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ3">ラバーキャスト</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ4">亜鉛キャスト</a>
					</li>					
				</ul><!-- end sub-link-all -->
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">インクジェット印刷</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content3_img1.jpg" alt="print" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/print_content3_img2.jpg" alt="print" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content3_img3.jpg" alt="print" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">４色フルカラーでイラストやデザインを表現できます。写真やグラデーションの印刷にも対応できます。金属への印刷のため、紙への印刷と比べると少し荒く見えるかもしれません。色数が多い場合や、細かいデザイン・イラストに最適で、同人グッズや販促イベントグッズなどによく活用されています。</div>
				<h3 class="sub-link-title"><i class="fa fa-circle-o"></i>この着彩が可能なピンバッジタイプ</h3>
				<ul class="sub-link-all clearfix">
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type1">真鍮プレス</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type2">真鍮エッチング</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ3">ラバーキャスト</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ4">亜鉛キャスト</a>
					</li>					
				</ul><!-- end sub-link-all -->
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">インクジェット印刷＋エポキシ（樹脂加工）</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content4_img1.jpg" alt="print" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/print_content4_img2.jpg" alt="print" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content4_img3.jpg" alt="print" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">オフセット印刷をしたピンバッジの表面に透明のエポキシ樹脂を乗せて仕上げます。表面がぷっくりと盛り上がった仕上がりになり、重厚感が出ます。より高級感のあるオリジナルピンバッジに仕上げたい方にお勧めです。</div>
				<h3 class="sub-link-title"><i class="fa fa-circle-o"></i>この着彩が可能なピンバッジタイプ</h3>
				<ul class="sub-link-all clearfix">
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type1">真鍮プレス</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type2">真鍮エッチング</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ3">ラバーキャスト</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ4">亜鉛キャスト</a>
					</li>					
				</ul><!-- end sub-link-all -->
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">印刷なし</h2>
		<div class="sub-content clearfix"><!-- begin sub-content -->
			<div class="sub-content-img">
				<div class="sub-img-left">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content5_img1.jpg" alt="print" /><p>
				</div>
				<div class="sub-img-right">
					<p class="sub-img-space"><img src="<?php bloginfo('template_url'); ?>/img/content/print_content5_img2.jpg" alt="print" /><p>
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/print_content5_img3.jpg" alt="print" /><p>
				</div>				
			</div><!-- end sub-content-img -->
			<div class="sub-content-text">
				<h3 class="sub-title">特長</h3>
				<div class="sub-text">着彩をせずに、金属やメッキの質感を生かしたオリジナルピンバッジの制作も可能です。着彩をしませんので、その分お値段も抑えられます。</div>
				<h3 class="sub-link-title"><i class="fa fa-circle-o"></i>この着彩が可能なピンバッジタイプ</h3>
				<ul class="sub-link-all clearfix">
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type1">真鍮プレス</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#type2">真鍮エッチング</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ3">ラバーキャスト</a>
					</li>
					<li>
						<i class="fa fa-caret-right"></i>
						<a href="<?php bloginfo('url'); ?>/type#typ4">亜鉛キャスト</a>
					</li>					
				</ul><!-- end sub-link-all -->
			</div><!-- end sub-content-text -->
		</div><!-- end sub-content -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="step-content-bg clearfix">			
				<a class="step-info-left" href="<?php bloginfo('url'); ?>/color">
					<i class="fa fa-angle-left"></i>					
					<span class="step-right">Step3</span>
					メッキの色を選ぶ
				</a>	
			
				<a class="step-info-right" href="<?php bloginfo('url'); ?>/catch">
					<span class="step-left">Step5</span>				
					<span class="pl10">留め具を選ぶ</span>
					<i class="fa fa-angle-right"></i>
				</a>
		</div><!-- end step -->
	</div><!-- end primary-row -->
<?php get_footer(); ?>