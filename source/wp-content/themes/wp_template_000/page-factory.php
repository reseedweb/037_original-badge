<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="factory-top-content">
		<h1 class="sub-top-title">工場のご紹介
	</div>
	<div class="sub-top-text">
		<p>オリジナルピンバッジ工房はピンバッジ制作のほとんどの作業を自社工場で行っております。<br />
		それにより余計な中間マージンが入らず低価格でピンバッジを提供することが可能です。<br />
		ここではその自社工場の様子を一部写真と動画でご紹介いたします。
		</p>
	</div>	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">自社工場での製造の様子</h2>		
		<div class="message-group message-col370 mt20"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img1.jpg" alt="factory" />
					</div><!-- end image -->    
					<h3 class="factory-title">金属の板をプレスする作業を行っています。</h3>
				</div><!-- end message-col -->
				<div class="message-col">					
					<div class="image">
						<iframe width="370" height="240" src="https://www.youtube.com/embed/Tv3QotZ9_kk" frameborder="0" allowfullscreen></iframe>
					</div><!-- end image -->    
					<h3 class="factory-title">デザインを金型に押しつける事で刻印します。</h3>
				</div><!-- end message-col -->				           
			</div><!-- end message-row -->
			
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img2.jpg" alt="factory" />
					</div><!-- end image -->    
					<h3 class="factory-title">こちらはプレス機による成型作業です。</h3>
				</div><!-- end message-col -->
				<div class="message-col">					
					<div class="image">
						<iframe width="370" height="240" src="https://www.youtube.com/embed/Tv3QotZ9_kk" frameborder="0" allowfullscreen></iframe>
					</div><!-- end image -->    
					<h3 class="factory-title">ロット数によって手作業と機械を振り分けています。</h3>
				</div><!-- end message-col -->				           
			</div><!-- end message-row -->
			
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img3.jpg" alt="factory" />
					</div><!-- end image -->    
					<h3 class="factory-title">金属板はテープ上に巻き取られて保管されています。</h3>
				</div><!-- end message-col -->
				<div class="message-col">					
					<div class="image">
						<iframe width="370" height="240" src="https://www.youtube.com/embed/Tv3QotZ9_kk" frameborder="0" allowfullscreen></iframe>
					</div><!-- end image -->    
					<h3 class="factory-title">ピンバッジ以外にも釦や金属製品の製造が得意です。</h3>
				</div><!-- end message-col -->				           
			</div><!-- end message-row -->
			
		</div><!-- end message-group -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->   
		<div class="top-part-contact clearfix">		
			<div class="top-part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="top" />
				</a>
			</div>
		</div><!-- /top-part-contact -->	
	</div><!-- end primary-row -->	
<?php get_footer(); ?> 