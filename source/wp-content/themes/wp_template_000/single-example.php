<?php get_template_part('header'); ?>
<div class="primary-row clearfix">		
	<?php get_template_part('part','breadcrumb');?>
	<!--<div class="example-top-content">
		<h1 class="sub-top-title"><span class="sub-title-step">Step5</span>留め具を選ぶ</h1>
	</div> -->
	<div class="example-content clearfix">
		<div class="example-content-info">   			
			<h2 class="h2-title"><?php the_title(); ?></h2>
			<div class="message-left message-340 clearfix">
				<div class="image">
					<?php the_post_thumbnail('example'); ?>
				</div>
				<div class="text">
					<div class="example-ctf-table">					
					<table>		
						<tr>
							<th>箱形状</th>
							<td><?php echo get_field('ctf_01', get_the_id()); ?></td>
						</tr>
						<tr>
							<th>厚み</th>
							<td><?php echo get_field('ctf_02', get_the_id()); ?></td>
						</tr>
						<tr>
							<th>表面色</th>
							<td><?php echo get_field('ctf_03', get_the_id()); ?></td>
						</tr>
						<tr>
							<th>印刷面</th>
							<td><?php echo get_field('ctf_04', get_the_id()); ?></td>
						</tr>
																				
					</table>
					</div>
				</div>
			</div>
			<div class="content-text">
				<?php echo apply_filters('the_content', $example_post->post_content); ?>
			</div>
		</div><!-- example-content-info -->
	</div><!-- end example-content -->		    
       
    <div class="navigation cf float">
		<?php if( get_previous_post() ): ?>
		<div class="single-blog-l"><?php previous_post_link('%link', '« %title'); ?></div>
		<?php endif;
		if( get_next_post() ): ?>
		<div class="single-blog-r"><?php next_post_link('%link', '%title »'); ?></div>
		<?php endif; ?>
    </div>
    <!-- /post navigation -->	
</div><!-- primary-row -->
<?php get_template_part('part','contact') ;?>
<?php get_template_part('footer'); ?>