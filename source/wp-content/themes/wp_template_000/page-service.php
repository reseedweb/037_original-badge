<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="datas-top-content">
		<h1 class="sub-top-title">代理店様サポート</h1>
	</div>
	<div class="sub-top-text">
		<p>お取り次ぎ店・イベント会社様にお得なサービスをご紹介しております。</p>
		<p>オリジナルピンバッジの製作メーカーを探している代理店様、担当者さまのお手伝いをいたします。</p>
		<p>ぜひご相談ください。</p>
	</div>	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->    
		<h2 class="h2-title">オリジナルピンバッジ制作をお考えの代理店様へ</h2>		
		<p class="service-top-text1 mt20"><span class="service-text-small">オリジナルピンバッジ工房では、</span><span class="service-text-large">お客様ひとりひとり</span><span class="service-text-small">に</span></p>
		<p class="service-top-text2 pt10"><span class="service-text-large">プロの担当者</span><span class="service-text-small">がつき、</span><span class="service-text-small">企画から納品まで</span><span class="service-text-large">完全サポート</span><span class="service-text-small">いたします。</span></p>
		<p class="service-top-text3">弊社はオリジナルピンバッジの製作を中心に多くの販促会社様・イベント会社様・SP広告代理店様に<br />パートナーとしてお選びいただいております。その理由は、自社工場での生産による確かな品質と、<br />長年の製作実績に基づいた柔軟な対応にございます。</p>
		<div class="service-top-text4">
			<p>オリジナルグッズやピンバッジの製作を頼みたいけれど、<br />どこに頼んだらいいか迷っている担当者の方へ。</p>
			<p>「オリジナルピンバッジ工房」はメーカーならではの<br />目線で担当者様のご負担を減らし、その先の<br />クライアント様に本当に喜んで頂けるご提案をいたします。</p>
		</div>
		<div class="service-content1 clearfix">
			<h3 class="service-content1-title">たとえばこんなお悩みを<br />解決いたします！</h3>
			<div class="service-content1-human"><img src="<?php bloginfo('template_url'); ?>/img/content/service_content1_human.jpg" alt="service" /></div>
			<ul class="service-content-list1">	
				<li>プレゼン、提案用の<br />サンプルが欲しい</li>
				<li>クライアント様の<br />予算内に抑えたい</li>				
				<li>デザイン、素材の<br />選択に迷っている</li>
			</ul>
			<ul class="service-content-list2">	
				<li>完成品と同じ仕様のサンプル<br />製作が可能です。ただし製作には<br />２～３週間ほどの期間をいただきます。<br />事前にご相談ください。</li>
				<li>差し支えなければお見積もりの際に<br />ご予算をスタッフまでお伝えください。<br />可能な限り予算内に収まるご提案を<br />差し上げます。</li>				
				<li>知識豊富な熟練スタッフがお客様の<br />ご要望をお聞きし、お勧めの製法や<br />仕様をご提案いたします。メーカー<br />ならではの発想で企画のお手伝いを<br />致します。</li>
			</ul>
		</div><!-- end service-content1 -->
	</div><!-- end primary-row -->

	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="top-part-contact clearfix">		
			<div class="top-part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="flow" />
				</a>
			</div>
		</div><!-- /top-part-contact -->
	</div><!-- end primary-row -->			
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">ピンバッジ以外にもグッズ制作のご相談お待ちしております</h2>
		<div class="service-content2 clearfix">
			<div class="service-content2-text">
				弊社は長年服飾の釦や社章などを製作し、金属加工の経<br />験と知識には他社様には負けない自信を持っておりま<br />す。ピンバッジ以外でも金属製品の製作・加工ならぜひ<br />わたしたちにお任せください。
			</div>
		</div><!-- end service-content2 -->
		<div class="service-content2-info">
			<h3 class="service-content2-title">例えばこんなグッズが制作できます</h3>
			<div class="service-info clearfix"><!-- begin service-info -->
				<div class="service-col">
					<div class="service-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_content2_img1.jpg" alt="service" />
					</div>			
					<h3 class="service-title">婦人服金属釦</h3>							
				</div><!--/service-col -->	

				<div class="service-col">
					<div class="service-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_content2_img2.jpg" alt="service" />
					</div>			
					<h3 class="service-title">バックル</h3>							
				</div><!--/service-col -->	
				
				<div class="service-col">
					<div class="service-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_content2_img3.jpg" alt="service" />
					</div>			
					<h3 class="service-title">アクセサリー</h3>					
				</div><!--/service-col -->	
			</div><!-- end service-info -->
			<p class="serive-text">他にも諸官庁制服釦、紳士服金属釦、婦人服金属釦、バックル、ベルト、<br />ステーショナリーグッズ、ブローチ、缶バッチ、靴・鞄の付属、学生服釦、ルアー…などなど</p>
		</div>
	</div><!-- end primary-row -->			
<?php get_footer(); ?> 