
                    </main><!-- end primary -->
                    <aside class="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside>                            
                </div><!-- end wrapper -->            
            </section><!-- end content -->

            <footer><!-- begin footer -->
                <div class="wrapper footer-content1 clearfix"><!-- begin footer-content1 -->                    
                    <div class="footer-logo">
						<a href="<?php bloginfo('url'); ?>/">
							<img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.png" alt="top" />
						</a>
					</div>
					<div class="footer-status">オリジナルピンバッジの<br />制作ならおまかせください！</div>
					<div class="footer-tel">
						<img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" alt="top" />
					</div>
					<div class="footer-con">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/common/footer_con.jpg" alt="top" />
						</a>						
					</div>
					<div class="footer-estimation">
						<a href="<?php bloginfo('url'); ?>/estimation">
							<img src="<?php bloginfo('template_url'); ?>/img/common/footer_estimation.jpg" alt="top" />
						</a>												
					</div>
                </div><!-- end footer-content1 -->
				
                <div class="wrapper footer-content2 clearfix"><!-- begin footer-content2 -->
					<ul class="footer-navi1">
						<li><a href="<?php bloginfo('url'); ?>/">トップページ</a></li>
						<li><a href="<?php bloginfo('url'); ?>/about">はじめての方へ</a></li>
						<li><a href="<?php bloginfo('url'); ?>/price">商材ラインナップ</a></li>
						<li><a href="<?php bloginfo('url'); ?>/datas">フルオーダー　</a></li>
						<li><a href="<?php bloginfo('url'); ?>/flow">ご注文の流れ</a></li>
						<li><a href="<?php bloginfo('url'); ?>/factory">工場設備の紹介</a></li>
						<li><a href="<?php bloginfo('url'); ?>/faq">よくある質問</a></li>						
					</ul>
                </div><!-- end footer-content2  -->
				
				<div class="wrapper footer-content3 clearfix"><!-- begin footer-content3 -->
                    <ul class="footer-navi2">
						<li><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/company">会社概要</a></li>
						<li><i class="fa fa-caret-right"></i><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー </a></li>
					</ul>
					<div class="footer-copyright">Copyright (c) 2015 オリジナルピンバッジ工房 Co.,Ltd. All Rights Reserved.</div>
                </div> 
            </footer><!-- end footer-content3 -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>
        <!-- js plugins -->
        <script type="text/javascript">

            jQuery(document).ready(function(){
                // dynamic gNavi
                var gNavi_item_count = $('#gNavi .dynamic > li').length;
                if(gNavi_item_count > 0){
                    var gNavi_item_length = $('#gNavi .dynamic').width() / parseInt(gNavi_item_count);                    
                    $('#gNavi .dynamic > li').css('width', Math.floor( gNavi_item_length ) + 'px' )
                }
                $('#gNavi .dynamic > li').hover(function(){
                    $(this).addClass('current');
                },function(){
                    $(this).removeClass('current');
                });                
				
				//top-slider-img
				$('#top-slider').scrollbox({
					linear: true,
					step: 2,
					delay: 3,/* delay time */
					speed: 0,                
					direction: 'h',                
				});           
				$('#top-slider-backward').click(function () {
				  $('#top-slider').trigger('backward');
				});
				$('#top-slider-forward').click(function () {
				  $('#top-slider').trigger('forward');       
				});   
				
				
            }); 
        </script>                
    </body>
</html>