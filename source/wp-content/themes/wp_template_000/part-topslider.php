<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">ピンバッジ制作事例</h2>
	<div id="slider">
        <div class="slider-nav-item slider-nav-left">
            <a id="top-slider-backward" href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a>
        </div>
        <div class="slider-nav-item slider-nav-right">
            <a id="top-slider-forward" href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a>
        </div>
        <div id="top-slider" class="slider-items clearfix">
			<?php	
			$posts = get_posts(array(
				'post_type'=> 'example',				
			));?>			
            <ul>
			<?php
			$i = 0;
			foreach($posts as $post):
			?>	
			<?php $i++; ?>
				<li>
					<a href="<?php echo get_permalink( $post->ID ); ?>/">
                        <?php echo get_the_post_thumbnail($post->ID,'thumbnail'); ?>
	                </a>
				</li><!-- end item -->            
			<?php endforeach; ?>				
            </ul>
        </div><!-- end slider-items -->
    </div><!-- end slider -->                   
</div><!-- end primary-row -->