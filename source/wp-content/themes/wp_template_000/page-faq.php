<?php get_header(); ?>
	<div class="primary-row clearfix">
		<div class="primary-row clearfix">
			<h2 class="h2-title">作り方が何通りかあるようですが、どのように選べばよいでしょうか？</h2>
			<p class="pt20">オリジナルピンバッジの製作目的やデザインによって、製法に向き不向きがございます。まずはサンプルの写真を参考にお好きなイメージに合うのものをお選びください。デザイン提出いただいてからより最適なピンバッジ製法を、弊社の専門スタッフからご提案させていただくこともございます。</p>			
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">
			<h2 class="h2-title">社章は作れますか？</h2>
			<p class="pt20">もちろん可能です。弊社は社章の製作事例も豊富にございますので、ぜひご相談ください。</p>			
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">
			<h2 class="h2-title">ピンバッジ制作にいくらぐらいかかるのでしょうか？</h2>
			<p class="pt20">オリジナルピンバッジは、その仕様・デザイン・数量などにより価格が大きく変わってまいります。ですので一概にピンバッジ製作にいくらかかるとは言えません。</p>
			<p>目安として、よく選ばれる仕様でピンバッジを製作した場合の参考価格（１個あたりの単価）を「参考価格例」のページに提示しております。</p>
			<p>この度お客様が製作を希望されているオリジナルピンバッジに関しては、随時個別にお見積もりをお出しいたします。まずはお電話か当サイトのお見積もりフォームよりご希望のピンバッジのイメージをお伝えください。</p>			
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">
			<h2 class="h2-title">どのくらいの期間で制作・納品出来ますか？</h2>
			<p class="pt20">オリジナルピンバッジの仕様や数量にもよりますが、データ校了から通常３〜４週間で商品発送が可能です。量産の前にサンプルを製作される場合はさらに２週間〜３週間プラスで期間をいただきます。ご了承ください。</p>
			<p>詳しい納期に関してはお見積もりの際に随時お答えいたします。</p>			
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">
			<h2 class="h2-title">入稿の方法をおしえてください。</h2>
			<p class="pt20">お客様自身でデザインデータを作成し、下記メールアドレスに添付送信してご入校ください。<br />data@original-badge.com</p>
			<p class="pt10">もし、ご自身でデザインデータを製作することが難しい場合は、ご希望のイメージから弊社でデータを作成することも可能です。内容によっては別途費用が発生する場合もございます。あらかじめご了承ください。</p>			
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">
			<h2 class="h2-title">写真をお送りして、それをバッジにすることは可能ですか？</h2>
			<p class="pt20">はい。可能でございます。ただし、データ変換料が発生する場合がございます。ピンバッジのタイプは「印刷タイプ」をお選びください。</p>			
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">
			<h2 class="h2-title">どのくらい細かい表現ができますか？</h2>
			<p class="pt20">ピンバッジの製法により異なります。印刷タイプであれば、デザインをそのままピンバッジにフルカラー印刷することが可能です。エッチング・プレス・キャスト製法をご希望の場合は、デザイン原案をみて確認させていただくのがベストかと思います。</p>
			<p>よろしければ当サイトの製作事例集を参考にご覧ください。</p>			
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">
			<h2 class="h2-title">最小ロット数はいくつですか？</h2>
			<p class="pt20">最小ロット300個から承っております。</p>			
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">
			<h2 class="h2-title">費用はどのタイミングで支払いになりますか？</h2>
			<p class="pt20">基本的にご注文時に前払い銀行振込でのお支払いをお願いしております。お振込を確認させていただき次第、製造に入らせていただきます。</p>			
		</div><!-- end primary-row -->		
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>