<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb');?>
	<div class="glossary-top-content">
		<h1 class="sub-top-title">ピンバッジ用語集</h1>
	</div>
	<div class="sub-top-text">
		<p>オリジナルピンバッジの製作でよくある製法や、よく出てくる専門用語について解説をしています。</p>
		<p>当サイトのオーダーガイドでは掲載していない製法や印刷方法についても、加えて説明をしています。</p>
		<p>もちろんどれも弊社で製作可能な内容ですので、もし気になる製法や印刷などあればお気軽にお申し付けください。</p>
	</div>		　　　　　　　　　　
　　　　　　
	<div class="glossary-list clearfix">
		<h2 class="glossary-list-title">ピンバッジ用語一覧</h2>
		<div class="glossary-list-info clearfix">
			<ul class="glossary-list-info1 clearfix">
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary1">真鍮プレス</a>
				</li>
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary2">ラバーキャスト</a>
				</li>
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary3">亜鉛キャスト</a>
				</li>
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary4">本七宝</a>
				</li>
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary5">擬似七宝</a>
				</li>
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary6">ラッカー</a>
				</li>
			</ul><!-- end glossary-list-info1 -->
			<ul class="glossary-list-info2 clearfix">
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary7">エポキシ</a>
				</li>
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary8">インクジェット印刷</a>
				</li>
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary9">シルク印刷</a>
				</li>
				<li>	
					<i class="fa fa-caret-right"></i>
					<a href="<?php bloginfo('url'); ?>/glossary#glossary10">シール印刷</a>
				</li>				
			</ul><!-- end glossary-list-info2 -->
		</div><!-- end glossary-list-info -->	
	</div><!-- end glossary-list -->
	
	<div id="glossary1" class="primary-row clearfix"><!-- begin primary-row -->  
		<h2 class="h2-title mt20">製造方法</h2>
		<div class="glossary-content clearfix">
			<h3 class="glossary-title">真鍮エッチング</h3>
			<div class="glossary-text">
				<p>エッチングとは、金属を化学反応によって意図的に腐食させ凹凸を作る技法で、主に銅版画などに利用されています。</p>
				<p>その技法をピンバッジに生かして制作するのが、<br />真鍮エッチングのオリジナルピンバッジです。</p>
				<p class="glossary-space">金属プレートの表面を薬品で溶かして溝を作り、凹ませた部分に色を入れてデザインを表現します。インク同士が混ざらないように、インクの境目には必ず0.6mm以上の縁が必要です。</p>
				<p>金属の腐食によりデザインを再現するので、あまりにも細かい<br />線は溶けてしまい表現できないことがあります。</p>
				<p class="glossary-space">プレス製法に比べて、柔らかい線で細密なデザインが表現でき、線の幅や表情も強弱をつけることができます。凹凸が少ないのも特徴です。凹凸をつける製法の中でも、安価に制作することができるのでコストを抑えたい方にお勧めな製法です。</p>
			</div>
		</div><!-- end glossary-content -->
	</div><!-- end primary-row -->
	
	<div id="glossary2" class="primary-row clearfix"><!-- begin primary-row -->  		
		<div class="glossary-content clearfix">
			<h3 class="glossary-title">真鍮プレス</h3>
			<div class="glossary-text">
				<p>プレス製法は、またの名をスタンププレス工法とも呼ばれ、その名の通りデザインを金属プレートに押し付けて制作します。</p>
				<p class="glossary-space">ピンバッジのオリジナルデザインの金型を作り、それを帯状も金属プレートに約１トンの圧力をかけて押し付け、<br />刻印します。金型はデザインの再現性を左右する重要な部分なので、職人の熟練した技と経験が必要になってきます。</p>
				<p>エッチングよりも線幅を細くすることが可能で、溝の深さもエッチングより深く立体感がでます。</p>
				<p>よりハッキリとしたデザインや凹凸を表現したいときにお勧めです。</p>
				<p>またエポキシ樹脂を乗せることで、表面をなめらかに仕上げることも可能です。</p>
				<p class="glossary-space">質感に重厚さを感じさせるため、社章や記念品のオリジナルピンバッジなどに選ばれることが多いです。</p>
			</div>
		</div><!-- end glossary-content -->
	</div><!-- end primary-row -->
	
	<div id="glossary3" class="primary-row clearfix"><!-- begin primary-row -->  		
		<div class="glossary-content clearfix">
			<h3 class="glossary-title">ラバーキャスト</h3>
			<div class="glossary-text">
				<p>オリジナルデザインに基づいてラバー（シリコン）の型を作り、そこにPVC樹脂を流し込んで鋳造します。</p>
				<p>文字やロゴ、イラスト、キャラクターなどを立体的に表現でき、アピール度抜群のオリジナルピンバッジが制作できます。</p>
				<p class="glossary-space">表面は立体的になり、裏面は平らになります。ラバー型なので、亜鉛キャストより型代が安く、小ロットからの生産が可能です。素材の性質上、亜鉛キャストよりもシャープな仕上がりにはできないので、小さい文字や細い部品の再現は苦手です。</p>
				<p>仕上がりは弾力がありゴムのように温かみのある感触です。</p>
				<p class="glossary-space">コロンと可愛らしく仕上がるので、キャラクターピンバッジや販促グッズなどにお勧めです。</p>
			</div>
		</div><!-- end glossary-content -->
	</div><!-- end primary-row -->
	
	<div id="glossary4" class="primary-row clearfix"><!-- begin primary-row -->  		
		<div class="glossary-content mb30 clearfix">
			<h3 class="glossary-title">亜鉛キャスト</h3>
			<div class="glossary-text">
				<p>立体の金型に亜鉛合金を流し込んで成形します。</p>
				<p>細かなデザインでも細部まで立体的に表現したオリジナルピンバッジが制作できます。</p>
				<p class="glossary-space">亜鉛は比較的低温で溶ける上に、湯流れがよく金型に忠実な再現が可能です。</p>
				<p>ですので、文字やロゴ・イラスト・キャラクターなどの細かいデザインや立体物の再現に最適です。</p>
				<p>また半立体だけでなく、完全な立体も再現できます。</pz
				<p>ただし亜鉛という素材そのものには金属光沢などは期待できないので、ほとんどの場合はメッキや塗装をして仕上げます。</p>
				<p class="glossary-space">ラバーキャストよりも型代が多少高くなるものの、仕上がりの美しさや奥行きの深さは違った満足感を得られます。</p>
				<p>より細密な立体物・デザインのオリジナルピンバッジを制作されたい方にお勧めです。</p>
			</div>
		</div><!-- end glossary-content -->
		<div class="top-part-contact clearfix">		
			<div class="top-part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="flow" />
				</a>
			</div>
		</div><!-- /top-part-contact -->
	</div><!-- end primary-row -->
	
	<div id="glossary5" class="primary-row clearfix"><!-- begin primary-row -->  		
		<div class="glossary-content clearfix">
			<h3 class="glossary-title">本七宝</h3>
			<div class="glossary-text">
				<p>伝統的な工芸品をピンバッジに取り入れた最高級の着色方法です。独特の色鮮やかな仕上がりは、他の着彩方法では真似できません。</p>
				<p class="glossary-space">ガラスによく似た材料を１色毎ごとに700～800℃で熱成し、プレスで凸凹をつけた部分に流し込み定着させます。さらに色を入れた後に表面を研磨し、平らに仕上げます。</p>
				<p>非常に高級感のある仕上がりのため、社章などでよく使用されています。</p>
			</div>
		</div><!-- end glossary-content -->
	</div><!-- end primary-row -->
	
	<div id="glossary6" class="primary-row clearfix"><!-- begin primary-row -->  		
		<div class="glossary-content clearfix">
			<h3 class="glossary-title">擬似七宝</h3>
			<div class="glossary-text">
				<p>透明エポキシに塗料を合わせて作り、まるで本七宝のような質感を再現できます。色のバリエーションも豊富で、七宝の高級感と落ち着いた雰囲気を持ちながら、より低価格でピンバッジが作成できます。</p>
				<p class="glossary-space">本七宝と同じようにプレスした型に塗料を流し込むのですが、エポキシ樹脂と混ぜ合わせた塗料なのでやわらかく傷つきやすい欠点があります。しかし七宝よりも低い温度で固まり加工がしやすいので、七宝にくらべ色の自由度が高く、豊富な色の再現が可能です。表面を磨いてからメッキ加工をほどします。</p>
				<p>非常に高級感のある仕上がりです。</p>
			</div>
		</div><!-- end glossary-content -->
	</div><!-- end primary-row -->
	
	<div id="glossary7" class="primary-row clearfix"><!-- begin primary-row -->  		
		<div class="glossary-content clearfix">
			<h3 class="glossary-title">色挿し（ラッカー）</h3>
			<div class="glossary-text">
				<p>色をつける部分に凹をつけて、ラッカー塗料を流し込み色をつけます。色と色との境目には金属色の縁ができ、それぞれの色面が金属の縁に囲まれた仕上がりになります。縁と塗料を流し込んだ部分に多少の高低差ができ、表面の凸凹がわかる見た目になります。</p>
				<p>オーソドックスなピンバッジの着彩方法で、記念品・販促物・イベントグッズなどで使われます。</p>
				<p class="glossary-space">さらにラッカー塗料の上に透明なエポキシ樹脂を乗せて表面がぷっくりと盛り上がった形に仕上げることが可能です。重厚感が出て高級感が増します。</p>				
			</div>
		</div><!-- end glossary-content -->
	</div><!-- end primary-row -->
	
	<div id="glossary8" class="primary-row clearfix"><!-- begin primary-row -->  		
		<div class="glossary-content mb30 clearfix">
			<h3 class="glossary-title">エポキシ</h3>
			<div class="glossary-text">
				<p>凸凹の部分に、ラッカー塗料の代わりにカラーエポキシを入れることも可能です。表面がぷっくりと盛り上がって、かなり厚みの感じられる独特な仕上がりです。ポップな感じに仕上がるので、アクセサリーやイベントグッズとして選ばれることが多いです。</p>				
			</div>
		</div><!-- end glossary-content -->
		
		<div class="top-part-contact clearfix">		
			<div class="top-part-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/top/top_part_conbtn.jpg" alt="flow" />
				</a>
			</div>
		</div><!-- /top-part-contact -->
	</div><!-- end primary-row -->	
	
	<div id="glossary9" class="primary-row clearfix"><!-- begin primary-row -->  		
		<div class="glossary-content clearfix">
			<h3 class="glossary-title">インクジェット印刷</h3>
			<div class="glossary-text">
				<p>金属プレートに直接デザインを印刷します。ピンバッジに写真やグラデーションを表現するなら、インクジェット印刷をお選びください。CMYK（シアン・マゼンタ・イエロー・ブラック）の4色をかけ合わせることで、フルカラーやグラデーションの表現が可能です。製作には版代が別途かかります。</p>				
				<p class="glossary-space">色数の多いデザインやキャラクターの絵柄を入れた同人グッズの製作などに向いています。単価が安く、ノベルティグッズとして大量に製作・配布する場合にもお勧めです。</p>				
				<p class="glossary-space">バッジ表面はエポキシ樹脂をコーディングして、印刷面を保護することをお勧めしています。</p>
			</div>
		</div><!-- end glossary-content -->
	</div><!-- end primary-row -->
	
	<div id="glossary10" class="primary-row mb40 clearfix"><!-- begin primary-row -->  		
		<div class="glossary-content clearfix">
			<h3 class="glossary-title">シルク印刷</h3>
			<div class="glossary-text">
				<p>繊細なデザインを紙印刷と同じようにピンバッジに表現することができます。イラストやキャラクターのイメージを忠実に再現したい方におすすめです。ただしインクジェット印刷とは異なり、シルク印刷ではグラデーションの表現ができません。</p>				
				<p class="glossary-space">単価が安く大量生産でき、色数が少ない場合はインクジェット印刷よりも阪代を安く抑えられます。</p>				
				<p>バッジ表面はエポキシ樹脂をコーディングして、印刷面を保護することをお勧めしています。</p>
			</div>
		</div><!-- end glossary-content -->
	</div><!-- end primary-row -->
		
<?php get_footer(); ?> 