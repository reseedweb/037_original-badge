<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>        

        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.scrollbox.js" type="text/javascript"></script>
        <?php wp_head(); ?>
    </head>
    <body>     
        <div id="screen_type"></div>
        
        <div id="wrapper"><!-- begin wrapper -->                      

            <header><!-- begin header -->
                <div class="wrapper">
                    <div class="header-content clearfix">            
                        <div class="logo">
                            <a href="<?php bloginfo('url'); ?>/"><img alt="logo" src="<?php bloginfo('template_url'); ?>/img/common/logo.png" /></a>
                        </div><!-- /logo -->
                        <div class="header-info">
                            <div class="header-info1">
								<div class="header-info1-text">オリジナルピンバッジの<br />制作ならおまかせください！</div>
								<div class="header-info1-tel">
									<img src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" alt="top"/>									
								</div>
								<div class="header-info1-con">
									<a href="<?php bloginfo('url'); ?>/contact">
										<img src="<?php bloginfo('template_url'); ?>/img/common/header_con.jpg" alt="top"/>
									<a/>									
								</div>
								<div class="header-info1-estimation">
									<a href="<?php bloginfo('url'); ?>/estimation">
										<img src="<?php bloginfo('template_url'); ?>/img/common/header_estimation.jpg" alt="top"/>
									</a>
								</div>
							</div><!-- /header-info1 -->
							
							<?php get_template_part('part','headerNav')?>					
							
                        </div><!--- /header-info -->
                    </div><!-- /header-content -->                
                </div><!-- end wrapper -->
            </header><!-- end header -->
            
			<?php get_template_part('part','navi')?>
                        
            <section id="content"><!-- begin content -->
                <div class="wrapper two-cols-left clearfix"><!-- begin two-cols -->
                    <main class="primary"><!-- begin primary -->       