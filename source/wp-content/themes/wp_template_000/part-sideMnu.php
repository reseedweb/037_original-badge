<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<div id="sideMenu">
		<h2 class="sideMnu-title">オーダーガイド</h2>
		<ul class="sideMnu">
			<li><a href="<?php bloginfo('url'); ?>/type"><span class="sideMnu-step">Step1</span>タイプを選ぶ<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/shape"><span class="sideMnu-step">Step2</span>形状を選ぶ<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/color"><span class="sideMnu-step">Step3</span>メッキ色を選ぶ<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/print"><span class="sideMnu-step">Step4</span>着彩方法を選ぶ<i class="fa fa-play"></i></a></li>
			<li><a href="<?php bloginfo('url'); ?>/catch"><span class="sideMnu-step">Step5</span>留め具を選ぶ<i class="fa fa-play"></i></a></li>
		</ul>
	</div>
</div><!-- end sidebar-row -->